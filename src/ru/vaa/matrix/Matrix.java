package ru.vaa.matrix;

public class Matrix {
    public static void main(String[] args) {
        int s = 0;
        int s2 = 0;
        int[][] a = new int[4][4];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                a[i][j] = (int) (1 + (Math.random() * 9));
                if (a[i] == a[j]) {
                    s += a[i][j];
                }
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("Сумма главной диагонали: " + s);
        for (int i = 0; i < 4; i++) {
            for (int j = i; j < 4; j++) {
                s2 += a[i][j];
            }
        }
        System.out.println("Сумма верхнего треугольника: " + s2);
    }
}
