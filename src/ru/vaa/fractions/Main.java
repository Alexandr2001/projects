package ru.vaa.fractions;

import java.util.Scanner;

public class Main {
    private static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите уравнение вида: 1/3_+_3/4...");
        String fraction = scan.nextLine();
        String[] symbols = fraction.split(" ");
        String[] OunFraction = symbols[0].split("/");
        String[] TwoFraction = symbols[2].split("/");
        Fraction fraction1 = new Fraction(Integer.valueOf(OunFraction[0]), Integer.valueOf(OunFraction[1]));
        Fraction fraction2 = new Fraction(Integer.valueOf(TwoFraction[0]), Integer.valueOf(TwoFraction[1]));
        int i = 0;
        if (fraction1.getDenominator() == 0 || fraction2.getDenominator() == 0) {
            System.out.println("На ноль делить нельзя!");
        } else {
            do try {
                switch (symbols[1]) {
                    case "+":
                        fraction1 = Fraction.add(fraction1, fraction2);
                        break;
                    case "-":
                        fraction1 = Fraction.subtraction(fraction1, fraction2);
                        break;
                    case "*":
                        fraction1 = Fraction.multiplication(fraction1, fraction2);
                        break;
                    case "/":
                        fraction1 = Fraction.division(fraction1, fraction2);
                        break;
                    default:
                        System.out.println("Вы ввели не верный знак операции");
                        break;
                }
                Fraction.reductionOfFraction(fraction1);
                Fraction.fractionConclusion(fraction1);

                System.out.println("\nВы хотите продолжить? \n1-да, нет - любое другое число");
                i = scan.nextInt();
                if (i == 1) {
                    System.out.println("Введите новую дробь. Пример: _+_1/2");
                    scan.nextLine();
                    fraction = scan.nextLine();
                    symbols = fraction.split(" ");
                    TwoFraction = symbols[2].split("/");
                    fraction2 = new Fraction(Integer.valueOf(TwoFraction[0]), Integer.valueOf(TwoFraction[1]));
                    if (fraction2.getDenominator() == 0) throw new ArithmeticException();
                }
            } catch (ArithmeticException e) {
                System.out.println("На ноль делить нельзя!");
            } while (i == 1);
        }
    }
}

