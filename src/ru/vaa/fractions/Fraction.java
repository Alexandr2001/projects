package ru.vaa.fractions;

public class Fraction {
    private int numerator;
    private int denominator;


    Fraction(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public Fraction() {
        this.numerator = 1;
        this.denominator = 1;
    }

    public static Fraction add(Fraction fraction1, Fraction fraction2) {
        if (fraction1.denominator == fraction2.denominator) {
            fraction1.numerator = fraction1.numerator + fraction2.numerator;

        } else {
            fraction1.numerator = (fraction1.numerator * fraction2.denominator) + (fraction2.numerator * fraction1.denominator);

            fraction1.denominator = fraction1.denominator * fraction2.denominator;
        }
        return fraction1;
    }

    public static Fraction subtraction(Fraction fraction1, Fraction fraction2) {
        if (fraction1.denominator == fraction2.denominator) {
            fraction1.numerator = fraction1.numerator - fraction2.numerator;

        } else {
            fraction1.numerator = (fraction1.numerator * fraction2.denominator) - (fraction2.numerator * fraction1.denominator);

            fraction1.denominator = fraction1.denominator * fraction2.denominator;
        }
        return fraction1;
    }

    public static Fraction multiplication(Fraction fraction1, Fraction fraction2) {
        fraction1.numerator = fraction1.numerator * fraction2.numerator;

        fraction1.denominator = fraction1.denominator * fraction2.denominator;

        return fraction1;
    }

    public static Fraction division(Fraction fraction1, Fraction fraction2) {
        if (fraction2.numerator == 0) throw new ArithmeticException();
        fraction1.numerator = fraction1.numerator * fraction2.denominator;
        fraction1.denominator = fraction1.denominator * fraction2.numerator;
        return fraction1;
    }

    public static void fractionConclusion(Fraction fraction1) {
        if (fraction1.numerator > fraction1.denominator && (fraction1.numerator % fraction1.denominator > 0)) {
            System.out.println("Результат выражения равен: " + fraction1.numerator / fraction1.denominator + "(" + fraction1.numerator % fraction1.denominator + "/" + fraction1.denominator + ")");
        } else if (fraction1.numerator == fraction1.denominator) {
            System.out.println("Результат выражения равен: " + fraction1.denominator);
        } else if (fraction1.denominator == 1) {
            System.out.println("Результат выражения равен: " + fraction1.numerator);
        } else if (fraction1.numerator == 0) {
            System.out.println("Результат выражения равен: 0");
            fraction1.denominator = 0;
        } else {
            System.out.printf("Результат выражения равен %d/%d", fraction1.numerator, fraction1.denominator);
        }
    }


    public static void reductionOfFraction(Fraction fraction1) {
        for (int i = fraction1.numerator; i > 1; i--) {
            if ((fraction1.numerator % i == 0) && (fraction1.denominator % i == 0)) {
                fraction1.numerator /= i;
                fraction1.denominator /= i;
            }
        }
    }

    public int getDenominator() {
        return denominator;
    }
}
