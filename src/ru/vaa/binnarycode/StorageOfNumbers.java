package ru.vaa.binnarycode;

public class StorageOfNumbers {
    public static void main(String[] args) {
        int i1 = 389;
        inToBits(i1);

        int i2 = -389;
        String negativeIntBits = Integer.toBinaryString(i2);
        System.out.println("Разряды числа: " + negativeIntBits);

        double d = 75.38;
        doubleToNumber(d, "Число: %5.2f\n");

        double d2 = Math.PI;
        doubleToNumber(d2, "Число: %10.15f\n");

        double d3 = 5.0/7;
        lossOfPresition(d3);

    }

    private static void lossOfPresition(double value) {
        System.out.format("Число: %10.16f\n", value);

        value += 300000;
        System.out.format("Число: %10.16f\n", value);
    }

    private static void doubleToNumber(double number, String s) {
        String sResult = "";
        long numberBits = Double.doubleToLongBits(number);
        sResult = Long.toBinaryString(numberBits);
        System.out.println("Представление вещественного числа в формате чисел с плавающей точкой");

        System.out.format(s, number);
        System.out.println("Формат чисел с плавающей точкой:");
        System.out.println(number > 0 ? "0" + sResult : sResult);
    }

    private static void inToBits(int number) {
        String intBits = Integer.toBinaryString(number);
        System.out.println("Разряды числа: " + intBits);
    }
}
