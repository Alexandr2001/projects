package ru.vaa.uniquearray;

import java.io.*;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<String> arrayList = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("C:\\Work\\Projects\\src\\ru\\vaa\\uniquearray\\ArrayString.txt")); BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("C:\\Work\\Projects\\src\\ru\\vaa\\uniquearray\\newArrayString.txt"))) {
            String s = "";
            while ((s = bufferedReader.readLine()) != null) {
                arrayList.add(s);
            }
            for (int i = 0; i < arrayList.size(); i++) {
                for (int j = i + 1; j < arrayList.size(); j++) {
                    if (arrayList.get(i).equals(arrayList.get(j))) {
                        arrayList.remove(j);
                    }
                }
            }
            for (String anArrayList : arrayList) {
                bufferedWriter.write(anArrayList + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
