package ru.vaa.standartalgoritm;

public class Test {
    public static void main(String[] args) {
        int[] a = {9, 35, 64, 4, 1};
        int[] b = {1, 53, 15, 67, 3};
        int[] c = {5, 3, 6, 7, 2};
        int s = addSum(a) + addSum(b) - addSum(c);
        System.out.println("Сумма: " + s);
    }

    private static int addSum(int[] array) {
        int sum = 0;
        for (int i = 1; i < array.length; i++) {
            sum += array[i];
        }
        return sum;
    }
}
