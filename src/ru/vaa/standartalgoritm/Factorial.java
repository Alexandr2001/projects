package ru.vaa.standartalgoritm;

import java.util.Scanner;

public class Factorial {
    private static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите n : ");
        int n = scan.nextInt();
        System.out.println("(" + n + " + 5)! = " + factorial(n + 5));
    }

    /**
     * Метод принимающий параметр n+5 вычисляет его факториал
     * И возвращающий результат
     */
    private static int factorial(int n) {
        int y = 1;
        if (n == 0 || n == 1) {
            return y;
        } else if (n < 0) {
            System.out.println("У факториала нет отрицательных чисел!");
        } else {
            for (int i = 2; i <= n; i++) {
                y *= i;
            }
        }
        return y;
    }
}
