package ru.vaa.standartalgoritm;

public class EvenNumbers {
    public static void main(String[] args) {
        int[] a = {15, 19, 26, -22};
        if (isEven(a)){
            System.out.println("В массиве присутствуют четные числа. Сумма этих четных чисел: " + sumOfEven(a));
        } else {
            System.out.println("Нет четных чисел");
        }
    }

    private static int sumOfEven(int[] a) {
        int s = 0;
        for (int anA : a) {
            if (anA % 2 == 0) {
                s += anA;
            }
        }
        return s;
    }

    private static boolean isEven(int [] a){
    for (int anA : a) {
        if (anA % 2 == 0) {
            return true;
        }
    }
        return false;
}

}
