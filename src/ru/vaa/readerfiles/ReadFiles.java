package ru.vaa.readerfiles;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class ReadFiles {
    private static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Сколько у вас времени( целое число ): ");
        int t = scan.nextInt();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("C:\\Work\\Projects\\src\\ru\\vaa\\readerfiles\\file.txt"));
            ArrayList<Integer> array = new ArrayList<>();
            String m = "";
            while ((m = bufferedReader.readLine()) != null) {
                array.add(Integer.parseInt(m));
            }
            sorting(array);
            int sum = 0;
            for (int i = 0; i < t; i++) {
                sum += array.get(i);
            }
            System.out.println(sum);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void sorting(ArrayList<Integer> array) {
        for (int i = array.size() - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (array.get(i) < array.get(j + 1)) {
                    int n = array.get(j);
                    array.set(j, array.get(j + 1));
                    array.set(j + 1, n);
                }
            }
        }
    }
}
