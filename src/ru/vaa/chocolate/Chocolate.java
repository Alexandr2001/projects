package ru.vaa.chocolate;

import java.util.Scanner;

public class Chocolate {
    private static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Сколько у вас денег?");
        int money = scan.nextInt();
        checking(money);

        System.out.println("Сколько стоит шоколадка?");
        int price = scan.nextInt();
        checking(price);

        int wrap = money / price;
        System.out.println("Вы купили шоколадки на всю сумму денег, теперь у вас " + wrap + " оберток\nСколько оберток вы хотите обменять?");
        int n = scan.nextInt();
        while (n > wrap || n < 0) {
            System.out.println("Не забывайте, у вас только " + wrap + " оберток!");
            n = scan.nextInt();
        }
        int bonus = n / 3;
        System.out.println("Вы получили еще " + bonus + " шоколадок");
    }

    private static void checking(int i) {
        while (i < 1) {
            System.out.println("Сумма введена не корректно! Попробуйте ввести ее снова.");
            i = scan.nextInt();
        }
    }
}
